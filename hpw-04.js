//------------------------------Funciones practica 4
function crear_input(tipo, nombre){
    obj_input=document.createElement("input");
    obj_input.setAttribute("type",tipo);
    obj_input.setAttribute("name",nombre);
    return obj_input;
    
    }
    crear_input("text","text1");
    
    //----------------------------------------------------
   
function crear_select(nom_select,opciones){
    
    obj_select=document.createElement("select");
    obj_select.setAttribute("id",nom_select);
    
    var opcion = document.createElement("option");
    
    obj_select.appendChild(opcion);
    for(var i=0;i<opciones.length;i++){
    variable = new Option(opciones[i]);
     obj_select.options[i]=variable;
     }
     return obj_select;
    
}

 crear_select ("select1",[10, 20, 30]);
 
 //---------------------------------------------------------
    
function crear_fieldset(leyen,input_f){
    obj_fieldset=document.createElement("FIELDSET"); 
    var leyenda=document.createElement("legend");
    leyenda.innerHTML=leyen;
    
    
    obj_label=document.createElement("label");
    obj_label.setAttribute("for","field");
    
    fiel_input=document.createElement("input")
    fiel_input.setAttribute("id",input_f);
    
    obj_fieldset.appendChild(leyenda);
     obj_fieldset.appendChild(obj_label);
      obj_fieldset.appendChild(fiel_input);
    //var nuevo_form=document.createElement("form");
    return obj_fieldset;
}

crear_fieldset("leyenda","field");

//-----------------------------------------------------------
function crear_textarea(id_t,rows,cols){
    obj_text=document.createElement("textarea");
    obj_text.setAttribute("id",id_t,"rows",rows,"cols",cols);
    obj_text.setAttribute("rows",rows);
    obj_text.setAttribute("cols",cols);

    return obj_text;
}

crear_textarea("textarea1",4,50);
//---------------------------------------------------
function crear_button(tipo,value,id){
    obj_button=document.createElement("button");
    obj_button.setAttribute("type",tipo);
    obj_button.setAttribute("value",value);
    obj_button.setAttribute("id",id);
    return obj_button;
}

crear_button("submit","borrar","boton_1");

//---------------------------------------------------

function crear_progress(valor,maximo,id){
    
    obj_progress=document.createElement("progress");
    obj_progress.setAttribute("id",id);
    obj_progress.setAttribute("value",valor);
    obj_progress.setAttribute("max",maximo);
    return obj_progress;
}

crear_progress("22","10","progre1");

//---------------------------------------------------------

function crear_input_color(color, id){
    obj_color=document.createElement("input");
    obj_color.setAttribute("type","color");
    obj_color.setAttribute("id",id);
    obj_color.setAttribute("value",color);
    return obj_color;
    
    }
    crear_input_color("#e76252","inp_c1");
    
    //---------------------------------------------------------------------
    function crear_input_range(min,max,sted, id){
    obj_range=document.createElement("input");
    obj_range.setAttribute("type","range");
    obj_range.setAttribute("id",id);
    obj_range.setAttribute("min",min);
    obj_range.setAttribute("max",max);
    obj_range.setAttribute("step",sted);
    return obj_range;
    
    }
    crear_input_range("-5","5","1","inp_ra1");
    
    // -------------------------------------------------------------
    
    function crear_input_number(min,max,step, id){
    obj_number=document.createElement("input");
    obj_number.setAttribute("type","number");
    obj_number.setAttribute("id",id);
    obj_number.setAttribute("min",min);
    obj_number.setAttribute("max",max);
    obj_number.setAttribute("step",step);
    return obj_number;
    
    }
    crear_input_number("0","25","3","inp_num1");
    